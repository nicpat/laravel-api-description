<?php

namespace appnic\ApiDescription\Tests;


use appnic\ApiDescription\Resources\DescriptionResource;
use Illuminate\Support\Arr;
use Orchestra\Testbench\TestCase;

class DescriptionResourceTest extends TestCase
{
    public function getPackageProviders($app)
    {
        return ['appnic\ApiDescription\Providers\ApiDescriptionProvider'];
    }

    public function testResponseMetaContainsDescriptionFields()
    {
        $model = new TestModel([
            'name' => 'test',
            'type_id' => 1
        ]);

        $responseContent = json_decode((new DescriptionResource($model))->response()->content(), true);
        $description = Arr::get($responseContent, config('apidescription.key'));

        $this->assertArrayHasKey('meta', $description);
        $this->assertArrayHasKey('fields', $description);
        $this->assertArrayHasKey('actions', $description);
    }

}