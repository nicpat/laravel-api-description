<?php

namespace appnic\ApiDescription\Tests;

use appnic\ApiDescription\Contracts\Describable;
use appnic\ApiDescription\Description;
use Illuminate\Database\Eloquent\Model;

class TestModel extends Model implements Describable
{
    protected $fillable = ['name', 'type_id'];

    public function users() {
        $this->hasMany('App\User');
    }

    public function describe(Description $description)
    {
        $description->field('id')->type('Integer')->label('ID')->readonly();
        $description->field('name')->type('String')->label('Name');
        $description->field('type')->type('Select')->label('Type')->relation('types','name');
        $description->action('delete')->type('Delete')->label('Delete');
        $description->meta('icon', 'somepath');
    }
}