<?php


namespace appnic\ApiDescription\Tests;


use appnic\ApiDescription\Resources\DescriptionResource;

class InheritedResource extends DescriptionResource
{
    public function with($request)
    {
        return [
            'meta'=>['additionalKey'=>'additionalValue']
        ];
    }
}