<?php

namespace appnic\ApiDescription\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ApiDescriptionProvider extends ServiceProvider
{
    public function boot() {
        $this->publishes([
            __DIR__.'/../../config/apidescription.php' => config_path('apidescription.php')
        ]);
    }

    /**
     * Register bindings
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/apidescription.php', 'apidescription');
    }
}