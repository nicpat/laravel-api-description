<?php

namespace appnic\ApiDescription;

use appnic\ApiDescription\Traits\HasMeta;

class Action
{
    use HasMeta;

    public $label;
    public $type;
    public $showOnIndex = false;

    public function type(string $type) {
        $this->type = $type;
        return $this;
    }

    public function label(string $label) {
        $this->label = $label;
        return $this;
    }

    public function showOnIndex(bool $showOnIndex = true) {
        $this->showOnIndex = $showOnIndex;
        return $this;
    }

    public function toArray() {
        return [
            'label' => $this->label,
            'type' => $this->type,
            'showOnIndex' => $this->showOnIndex
        ];
    }
}