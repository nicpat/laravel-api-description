<?php

namespace appnic\ApiDescription;

use appnic\ApiDescription\Traits\HasMeta;

class Description
{
    use HasMeta;

    public $fields = [];
    public $actions = [];

    /**
     * Add a new field
     * @param string $name
     * @return Field
     */
    public function field(string $name) {
        $field = new Field();
        $this->fields[] = $field->name($name);
        return $field;
    }

    /**
     * Add a new action
     * @param string $type
     * @return Action
     */
    public function action(string $type) {
        $action = new Action();
        $this->actions[] = $action->type($type);
        return $action;
    }

    public function toArray() : array {
        $return = [
            'meta' => $this->meta
        ];

        foreach ($this->actions as $action) {
            $return['actions'][] = $action->toArray();
        }

        foreach ($this->fields as $field) {
            $return['fields'][] = $field->toArray();
        }

        return $return;
    }
}