<?php

namespace appnic\ApiDescription;

use appnic\ApiDescription\Traits\HasMeta;

class Field
{
    use HasMeta;

    public $name;
    public $label;
    public $readonly = false;
    public $type;
    public $showOnIndex = false;
    public $relation;

    public function name(string $name) {
        $this->name = $name;
        return $this;
    }

    public function label(string $label) {
        $this->label = $label;
        return $this;
    }

    public function readonly(bool $readonly = true) {
        $this->readonly = $readonly;
        return $this;
    }

    public function type(string $type) {
        $this->type = $type;
        return $this;
    }

    public function showOnIndex(bool $showOnIndex = true) {
        $this->showOnIndex = $showOnIndex;
        return $this;
    }

    public function relation(string $url, string $labelField, bool $many = false) {
        $relation = new Relation();
        $this->relation = $relation->url($url)->labelField($labelField)->many($many);
        return $this;
    }

    public function toArray() {
        $return = [
            'name' => $this->name,
            'label' => $this->label,
            'readonly' => $this->readonly,
            'type' => $this->type,
            'showOnIndex' => $this->showOnIndex
        ];

        if($this->relation != null) {
            $return['relation'] = $this->relation->toArray();
        }

        return $return;
    }
}